# Documentation

Technical documentation website - docs.actronika.com - made with MkDocs
=======
<!-- ![Build Status](https://gitlab.com/pages/mkdocs/badges/master/build.svg) -->

This project's static Pages are built by [GitLab CI][ci], with [MkDocs][mkdocs]
```
image: python:3.8-buster

before_script:
  - pip install -r requirements.txt

test:
  stage: test
  script:
  - mkdocs build --strict --verbose --site-dir test
  artifacts:
    paths:
    - test
  except:
  - master

pages:
  stage: deploy
  script:
  - mkdocs build --strict --verbose
  artifacts:
    paths:
    - public
  only:
  - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. clone this project
1. Install MkDocs with pip :
1. Preview your project: `mkdocs serve`,
   your site can be accessed under `localhost:8000`
1. Add content
1. Generate the website: `mkdocs build` (optional)

[ci]: https://about.gitlab.com/gitlab-ci/
[mkdocs]: http://www.mkdocs.org
